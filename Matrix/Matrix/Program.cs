﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                string[] file_list = Directory.GetFiles(args[0], "*.txt");
                foreach (string file_to_read in file_list)
                {
                    List<string[]> mat_txt_list = new List<string[]>();
                    string line;
                    List<Matrix> transp_mat = new List<Matrix>();
                    using (StreamReader sr = new StreamReader(file_to_read, System.Text.Encoding.Default))
                    {
                        Console.WriteLine("Считываю файл");
                        string command = sr.ReadLine();
                        if (command == "transpose")
                        {
                            while ((line = sr.ReadLine()) != " ")
                                {
                                    while (((line = sr.ReadLine()) != null))
                                    {
                                        if (line != "")
                                        mat_txt_list.Add(line.Split());
                                        else
                                        {
                                        transp_mat.Add(new Matrix(mat_txt_list));
                                        mat_txt_list.Clear();
                                        }
                                    }
                                    

                                if (line == null)
                                {
                                    transp_mat.Add(new Matrix(mat_txt_list));
                                    Console.WriteLine("Чтение файла завершено");
                                    Thread.Sleep(20);
                                    Matrix.save_result(file_to_read.Split('.')[0], Matrix.solve(command, null, null, transp_mat));
                                    Environment.Exit(0);
                                }
                            }
                        }
                        sr.ReadLine();
                        while ((line = sr.ReadLine()) != "")
                        {
                            mat_txt_list.Add(line.Split());
                        }

                         Matrix a = new Matrix(mat_txt_list);
                         mat_txt_list.Clear();
                         while ((line = sr.ReadLine()) != "" && ((line != null)))
                         {

                                mat_txt_list.Add(line.Split());
                         }
                         Matrix b = new Matrix(mat_txt_list);
                         Console.WriteLine("Чтение файла завершено");
                         Thread.Sleep(20);
                         Matrix.save_result(file_to_read.Split('.')[0], Matrix.solve(command, a, b));
                        
                    }
                    

                }
            }
            else
            {
                Console.WriteLine("Пожалуйста, укажите имя директории");
            }
        }
        public class Matrix
        {
            public int[,] matrix { get; set; }

            public Matrix(int[,] matrix)
            {
                this.matrix = matrix;
            }
         
            public Matrix(List<string[]> mat_txt_list)
            {
                matrix = new int[mat_txt_list.Count,mat_txt_list[0].Length];
                try
                {
                    for (int i = 0; i < mat_txt_list.Count; i++)
                    {
                        for (int t = 0; t < mat_txt_list[0].Length; t++)
                        {
                            matrix[i, t] = Convert.ToInt32(mat_txt_list[i][t]);
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(1);
                }
                

            }
            public  static List<Matrix> solve(string command, Matrix a=null, Matrix b=null,List<Matrix> mlist=null)
            {
                List<Matrix> res = new List<Matrix>();
                switch (command)
                 {
                     case "multiply":{
                            res.Add(a = a * b);
                            return res;
                        } ;
                     case "add": {
                            res.Add( a = a + b);
                            return res;
                        };
                     case "subtract": {
                            res.Add( a = a - b);
                            return res;
                        } ;
                     case "transpose":
                        {
                            foreach (Matrix mat in mlist)
                            {

                                res.Add( transpose(mat));
                            }
                            return res;
                        };
                     default: { return null; };
                 }
            }
            public static Matrix operator +(Matrix a, Matrix b)
            {
                int rows_a = a.matrix.GetUpperBound(0) + 1;
                int columns_a = a.matrix.Length / rows_a;
            
                try
                {
                    Console.WriteLine("Произвожу вычисления");
                    
                    for (int i = 0; i < rows_a; i++)
                    {
                        for (int t = 0; t < columns_a; t++)
                        {
                            a.matrix[i, t] = a.matrix[i, t] + b.matrix[i, t];
                        }
                        
                    }
                    Console.WriteLine("Вычисления завершены");
                    return a;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(1);
                    return null;
                    
                }
                
            }
            public static Matrix operator -(Matrix a, Matrix b)
            {
                int rows_a = a.matrix.GetUpperBound(0) + 1;
                int columns_a = a.matrix.Length / rows_a;

                try
                {
                    Console.WriteLine("Произвожу вычисления");
                    for (int i = 0; i < rows_a; i++)
                    {
                        for (int t = 0; t < columns_a; t++)
                        {
                            a.matrix[i, t] = a.matrix[i, t] - b.matrix[i, t];
                           
                        }
                    }
                    Console.WriteLine("Вычисления завершены");
                    return a;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(1);
                    return null;
                }
            }
            public static Matrix operator *(Matrix a, Matrix b)
            {
                int rows_a = a.matrix.GetUpperBound(0) + 1;
                int columns_a = a.matrix.Length / rows_a;

                int rows_b = b.matrix.GetUpperBound(0) + 1;
                int columns_b = b.matrix.Length / rows_b;
                int[,] c = new int[rows_b, columns_a];
                
                try
                {
                    Console.WriteLine("Произвожу вычисления");
                    for (int i = 0; i < rows_a; i++)
                    {
                        for (int t = 0; t < columns_b; t++)
                        {
                            for (int k = 0; k < rows_b; k++)
                                c[i, t] += a.matrix[i, k] * b.matrix[k, t];
                          
                        }
                    }
                    Console.WriteLine("Вычисления завершены");
                    a.matrix = c;
                    return a;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(1);
                    return null;
                }


            }
            public static Matrix transpose(Matrix a)
            {


                int rows_a = a.matrix.GetUpperBound(0) + 1;
                int columns_a = a.matrix.Length / rows_a;
                int[,] b = new int[columns_a, rows_a];
               
                try
                {
                    Console.WriteLine("Произвожу вычисления");
                    for (int i = 0; i < columns_a; i++)
                    {
                        for (int t = 0; t < rows_a; t++)
                        {
                            b[i, t] = a.matrix[t, i];
                          
                        }
                    }
                    a.matrix = b;
                    Console.WriteLine("Вычисления завершены");
                    return a;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Environment.Exit(1);
                    return null;
                }
            }
            public static void save_result(string path, List<Matrix> mlist)
            {

                using (StreamWriter outputFile = new StreamWriter(path + "_result.txt"))
                {
                    Console.WriteLine("Записываю в файл");
                    for (int index = 0; index < mlist.Count; index++)
                    {
                        int rows_mlist = mlist[index].matrix.GetUpperBound(0) + 1;
                        int columns_mlist = mlist[index].matrix.Length / rows_mlist;
                        for (int i = 0; i < rows_mlist; i++)
                        {

                            for (int t = 0; t < columns_mlist; t++)
                            {
                                outputFile.Write(mlist[index].matrix[i, t] + " ");
                            }
                            outputFile.WriteLine();
                        }
                        outputFile.WriteLine();
                    }
                    Console.WriteLine("Запись в файл завершена");
                }
            }
           
        }
    }
}
